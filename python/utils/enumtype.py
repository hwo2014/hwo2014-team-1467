def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    enums['keys'] = enums.keys()
    enums['values'] = enums.values()
    
    return type('Enum', (), enums)
