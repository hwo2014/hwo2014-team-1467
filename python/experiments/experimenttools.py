import os
import matplotlib.pyplot as plot

from commshandler.filecommshandler import FileCommsHandler
from driver.datagatheringdriver import DataGatheringDriver
from race.raceoptions import RaceOptions
from commshandler.networkcommshandler import NetworkCommsHandler
from driver.constantthrottledriver import Driver
from driver.driverfactory import DriverFactory
from driver.strategy import CommandStrategy

def get_race_data(directory, file_name):
    driver_factory = DriverFactory(DataGatheringDriver)
    comms_handler = FileCommsHandler(
            driver_factory, directory, file_name
    )
    comms_handler.run()
    
    return comms_handler.race.track, comms_handler.driver.race_data

def filter_car_data(race_data, car_color):
    car_data = []
    for race_state in race_data:
        car = race_state.cars[car_color]
        if car.car_position:
            car_data.append(car)
    
    return car_data

def get_car_data(directory, file_name, car_color):
    track, race_data = get_race_data(directory, file_name)
    car_data = filter_car_data(race_data, car_color)
    return track, car_data

def plot_quantity_over_ticks(data, x_label, y_label, title, show=False):
    plot.plot(data)
    plot.ylabel(x_label)
    plot.xlabel(y_label)
    plot.title(title)
    
    save_plot(title)
    if show:
        plot.show()

def save_plot(name):
    if not os.path.exists('../plots/'):
        os.makedirs('../plots/')
    plot.savefig('../plots/{0}.png'.format(name.replace(' ', '_')))

def run_with_commands(host, port, name, key, command_list):
    race_options = RaceOptions(name, key)
    
    strategy = CommandStrategy(command_list)
    driver_factory = DriverFactory(Driver, strategy, strategy)
    comms_handler = NetworkCommsHandler(
            host, port, race_options, driver_factory, True
    )
    comms_handler.run()
