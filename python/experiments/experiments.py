from experimenttools import plot_quantity_over_ticks, get_car_data

def plot_slip_angle_over_time(directory, file_name, car_color):
    track, car_data = get_car_data(directory, file_name, car_color)
    angle_data = map(
            lambda car: car.car_position.angle,
            filter(lambda car: car.car_position is not None, car_data)
    )
    
    plot_quantity_over_ticks(
            angle_data, 'Angle (degrees)', 'Time (ticks)',
            'Slip angle over time ({0}, {1})'.format(file_name, track.name),
            True
    )

def plot_speed_over_time(directory, file_name, car_color):
    track, car_data = get_car_data(directory, file_name, car_color)
    speed_data = map(
            lambda car: car.speed, car_data
    )
    
    plot_quantity_over_ticks(
            speed_data, 'Speed (m/tick)', 'Time (ticks)',
            'Speed over time ({0}, {1})'.format(file_name, track.name), True
    )

def plot_acc_over_time(directory, file_name, car_color):
    track, car_data = get_car_data(directory, file_name, car_color)
    acc_data = map(
            lambda car: car.acceleration, car_data
    )[0:500]
    
    plot_quantity_over_ticks(
            acc_data, 'Acceleration (m/tick^2)', 'Time (ticks)',
            'Acceleration over time ({0}, {1})'.format(file_name, track.name),
            True
    )

def approximate_c_div_m(directory, file_name, car_color):
    track, car_data = get_car_data(directory, file_name, car_color)
    speed_data = map(
            lambda car: car.speed, car_data
    )
    acc_data = map(
            lambda car: car.acceleration, car_data
    )
    
    i = 1
    c_div_ms = []
    while i < len(speed_data) and i < len(acc_data):
        initial_speed = speed_data[i - 1]
        acceleration = acc_data[i]
        denom = (25 - initial_speed ** 2)
        if denom > 0:
            c_div_m = acceleration / denom
            c_div_ms.append(c_div_m)
        i += 1
    
    print speed_data[0:10]
    print acc_data[0:10]
    print c_div_ms[0:10]
    print min(c_div_ms)
    print max(c_div_ms)
    print sum(c_div_ms[1:]) / len(c_div_ms[1:])

def predict_acceleration(top_velocity, current_velocity):
    return 0.004 * (top_velocity ** 2 - current_velocity ** 2)

import matplotlib.pyplot as plot
def plot_velocity_prediction():
    velocity = 0
    velocities = [velocity]
    accelerations = [0]
    for i in range(500):
        num_samples = 100
        for j in range(num_samples):
            acceleration = predict_acceleration(5.0, velocity) / num_samples
            velocity += acceleration
        
        velocities.append(velocity)
        accelerations.append(acceleration)
    
    #plot.plot(velocities)
    plot.plot(accelerations)
    plot.show()

if __name__ == '__main__':
    file_dir, file_name, color = '../log', 'constthrottle50_red', 'red'
    #approximate_c_div_m(file_dir, file_name, color)
    #plot_slip_angle_over_time(file_dir, file_name, color)
    #plot_speed_over_time(file_dir, file_name, color)
    plot_acc_over_time(file_dir, file_name, color)
    plot_velocity_prediction()
