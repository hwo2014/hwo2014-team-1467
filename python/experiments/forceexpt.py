import sys
from commshandler.clientcommands import Throttle
from experimenttools import run_with_commands

acc_decc_test = [
    (200, Throttle(0.5)),
    (400, Throttle(0.0)),
    (600, Throttle(0.5)),
    (800, Throttle(0.0)),
    (1000, Throttle(0.655)),
    (1200, Throttle(0.0)),
    (1400, Throttle(0.655)),
    (1600, Throttle(0.0)),
    (float('infinity'), Throttle(0.655))
]

throttle_levels_test = [
    (200, Throttle(0.2)),
    (400, Throttle(0.4)),
    (600, Throttle(0.6)),
    (float('infinity'), Throttle(0.655))
]

'Theory: force is proportional to square of throttle'
def calculate_force():
    return None

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: need args host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        run_with_commands(host, port, name, key, throttle_levels_test)
