class Driver(object):
    def __init__(self, name, comms_handler, installation_strategy,
                 race_strategy):
        self.name = name
        self.comms_handler = comms_handler
        
        self.strategy = installation_strategy
        self.installation_strategy = installation_strategy
        self.race_strategy = race_strategy
        
        self.installation_strategy.bot_name = name
        self.race_strategy.bot_name = name
    
    def on_join(self, data):
        print("Joined")
        return self.strategy.on_join(data)
    
    def on_game_start(self, data):
        print("Race started")
        return self.strategy.on_game_start(data)
    
    def on_game_init(self, data, race):
        self.race = race
        self.installation_strategy.race = race
        self.race_strategy.race = race
    
    def on_car_positions(self, data):
        return self.strategy.on_car_positions(data)
    
    def on_crash(self, data):
        print("Someone crashed")
        return self.strategy.on_crash(data)
    
    def on_error(self, data):
        print("Error: {0}".format(data))
        return self.strategy.on_error(data)
