from commshandler.clientcommands import Ping

class DataGatheringDriver(object):
    def __init__(self, name, comms_handler):
        self.name = name
        self.comms_handler = comms_handler
        self.race_data = []
    
    def on_join(self, data):
        return Ping()
    
    def on_game_start(self, data):
        return Ping()
    
    def on_game_init(self, data, race):
        return Ping()
    
    def on_car_positions(self, data):
        race = self.comms_handler.race.copy()
        self.race_data.append(race)
        return Ping()
    
    def on_crash(self, data):
        return Ping()
    
    def on_error(self, data):
        return Ping()
