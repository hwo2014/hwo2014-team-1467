from commshandler.clientcommands import Ping, Throttle

class ConstantThrottleStrategy(object):
    def __init__(self, throttle_setting):
        self.bot_name = None
        self.throttle_setting = throttle_setting
        self.race = None
    
    def on_join(self, data):
        return Ping()
    
    def on_game_start(self, data):
        return Ping()
    
    def on_car_positions(self, data):
        return Throttle(self.throttle_setting)
    
    def on_crash(self, data):
        return Ping()
    
    def on_error(self, data):
        return Ping()

class CommandStrategy(object):
    def __init__(self, commands):
        assert len(commands) > 0
        self.commands = commands
        self.command_tick_limit, self.command = self.commands.pop(0)
        self.race = None
    
    def on_join(self, data):
        return Ping()
    
    def on_game_start(self, data):
        return Ping()
    
    def on_car_positions(self, data):
        current_tick = self.race.tick
        if current_tick > self.command_tick_limit:
            if len(self.commands) > 0:
                self.command_tick_limit, self.command = self.commands.pop(0)
            else:
                self.command_tick_limit = float('infinity')
                self.command = Ping()
        
        return self.command
    
    def on_crash(self, data):
        return Ping()
    
    def on_error(self, data):
        return Ping()
