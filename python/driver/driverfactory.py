class DriverFactory(object):
    def __init__(self, driver_class, *args, **kwargs):
        self.driver_class = driver_class
        self.args = args
        self.kwargs = kwargs
    
    def create_driver_instance(self, bot_name, comms_handler):
        return self.driver_class(
                bot_name, comms_handler, *self.args, **self.kwargs
        )
