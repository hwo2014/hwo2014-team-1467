import sys
import time
import random

from multiprocessing import Process
from race.raceoptions import RaceOptions
from driver.constantthrottledriver import Driver
from driver.driverfactory import DriverFactory
from driver.strategy import ConstantThrottleStrategy
from commshandler.networkcommshandler import NetworkCommsHandler

if __name__ == '__main__':
    random.seed()
    if len(sys.argv) != 6:
        print("Usage: ./multibot host port botname botkey numbots")
    else:
        processes = []
        host, port, name, key, car_count = sys.argv[1:6]
        for i in range(int(car_count)):
            rand_throttle = random.random()
            race_options = RaceOptions(
                    name, key, "keimola", int(car_count), i, i == 0
            )
            
            strategy = ConstantThrottleStrategy(rand_throttle)
            driver_factory = DriverFactory(
                    Driver, strategy, strategy
            )
            comms_handler = NetworkCommsHandler(
                host, port, race_options, driver_factory, True
            )
            
            p = Process(target=comms_handler.run)
            processes.append(p)
            p.start()
            time.sleep(0.1)
        
        for p in processes:
            p.join()
    
    print("finished")
