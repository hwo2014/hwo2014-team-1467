import os

from commshandler import CommsHandler

class FileCommsHandler(CommsHandler):
    def __init__(self, driver_factory, directory, file_name):
        super(FileCommsHandler, self).__init__("", driver_factory, False)
        self.full_path = os.path.join(directory, file_name)
    
    def log_message(self, msg):
        pass
    
    def write_logged_messages(self):
        pass
    
    def create_input_source(self):
        return open(self.full_path, 'r+')
    
    def msg(self, msg_type, data):
        pass
    
    def send(self, msg):
        pass
    
    def join(self):
        pass
    
    def throttle(self, throttle):
        pass
    
    def ping(self):
        pass
