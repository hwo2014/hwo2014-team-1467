from abc import ABCMeta, abstractmethod

class ClientCommands(object):
    __metaclass__ = ABCMeta
    def __init__(self):
        pass
    
    @abstractmethod
    def execute(self, comms_handler):
        pass

class Throttle(object):
    def __init__(self, throttle_setting):
        self.throttle_setting = throttle_setting
    
    def execute(self, comms_handler):
        comms_handler.throttle(self.throttle_setting)

class Ping(object):
    def execute(self, comms_handler):
        comms_handler.ping()
