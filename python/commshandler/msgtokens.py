from utils.enumtype import enum

server_message_types = {
    'GAME_START': 'gameStart',
    'CAR_POSITIONS': 'carPositions',
    'CRASH': 'crash',
    'GAME_END': 'gameEnd',
    'ERROR': 'error',
    'YOUR_CAR': 'yourCar',
    'GAME_INIT': 'gameInit',
    'TOURNAMENT_END': 'tournamentEnd'
}

SMsgTypes = enum(**server_message_types)

client_message_types = {
    'JOIN': 'join',
    'CREATE_RACE': 'createRace',
    'JOIN_RACE': 'joinRace',
    'THROTTLE': 'throttle',
    'PING': 'ping',
}

CMsgTypes = enum(**client_message_types)

response_fields = {
    'MSG_TYPE': 'msgType',
    'DATA': 'data',
    'GAME_TICK': 'gameTick',
    'GAME_ID': 'gameId',
}
SResponse = enum(**response_fields)

create_race_fields = {
    'BOT_ID': 'botId',
    'TRACK_NAME': 'trackName',
    'CAR_COUNT': 'carCount',
    'PASSWORD': 'password',
}
CreateRace = enum(**create_race_fields)

car_id_fields = {
    'NAME': "name",
    'KEY': "key",
}
CCarIdSpec = enum(**car_id_fields)

race_spec_fields = {
    'TRACK': 'track',
    'CARS': 'cars',
    'RACE_SESSION': 'raceSession',
}
RaceSpec = enum(**race_spec_fields)

track_spec_fields = {
    'ID': 'id',
    'NAME': 'name',
    'PIECES': 'pieces',
    'LANES': 'lanes',
    'STARTING_POINT': 'startingPoint',
}
TrackSpec = enum(**track_spec_fields)

piece_spec_fields = {
    'LENGTH': 'length',
    'RADIUS': 'radius',
    'SWITCH': 'switch',
    'ANGLE': 'angle'
}
PieceSpec = enum(**piece_spec_fields)

race_session_fields = {
    'LAPS': 'laps',
    'MAX_LAP_TIME_MS': 'maxLapTimeMs',
    'QUICK_RACE': 'quickRace',
}
RaceSession = enum(**race_session_fields)

car_spec_fields = {
    'ID': 'id',
    'DIMENSIONS': 'dimensions',
}
CarSpec = enum(**car_spec_fields)

lane_spec_fields = {
    'DISTANCE_FROM_CENTER': 'distanceFromCenter',
    'INDEX': 'index',
}
LaneSpec = enum(**lane_spec_fields)

car_dimensions_fields = {
    'WIDTH': 'width',
    'LENGTH': 'length',
    'GUIDE_FLAG_POSITION': 'guideFlagPosition',
}
CarDimensions = enum(**car_dimensions_fields)

s_car_id_fields = {
    'COLOR': 'color',
    'NAME': 'name',
}
SCarIdSpec = enum(**s_car_id_fields)

position_spec_fields = {
    'ID': 'id',
    'ANGLE': 'angle',
    'PIECE_POS': 'piecePosition',
}
PositionSpec = enum(**position_spec_fields)

piece_position_fields = {
    'PIECE_INDEX': 'pieceIndex',
    'IN_PIECE_DISTANCE': 'inPieceDistance',
    'LANE': 'lane',
    'LAP': 'lap',
}
PiecePosition = enum(**piece_position_fields)

lane_fields = {
    'START': 'startLaneIndex',
    'END': 'endLaneIndex'
}
LanePosition = enum(**lane_fields)

game_init_data_fields = {
    'RACE': 'race'
}
GameInit = enum(**game_init_data_fields)
