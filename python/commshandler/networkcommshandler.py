import socket
import json
from msgtokens import SResponse, CMsgTypes, CreateRace, CCarIdSpec
from commshandler import CommsHandler

class NetworkCommsHandler(CommsHandler):
    def __init__(self, host, port, race_options, driver_factory, log=False):
        self.init_race_options(race_options)
        self.connect_socket(host, port)
        self.game_tick = None
        
        super(NetworkCommsHandler, self).__init__(
                self.bot_name, driver_factory, log
        )
    
    def init_race_options(self, race_options):
        self.key = race_options.key
        self.car_count = race_options.car_count
        self.create_race = race_options.create_race
        self.index = race_options.index
        self.bot_name = race_options.name + str(self.index)
        self.creation_password = race_options.password
        self.track = race_options.track
    
    def connect_socket(self, host, port):
        print("Connecting with parameters:")
        print(
            "host={0}, port={1}, bot name={2}, key={3}".format(
                    host, port, self.bot_name, self.key
            )
        )
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, int(port)))
    
    def create_input_source(self):
        return self.socket.makefile()
    
    def msg(self, msg_type, data):
        message = {SResponse.MSG_TYPE: msg_type, SResponse.DATA: data}
        if self.game_tick:
            message[SResponse.GAME_TICK] = self.game_tick
        self.log_message(message)
        
        self.send(json.dumps(message))
    
    def send(self, msg):
        self.socket.send(msg + "\n")
    
    def join(self):
        if not self.track:
            return self.msg(
                    CMsgTypes.JOIN,
                    { CCarIdSpec.NAME: self.bot_name, CCarIdSpec.KEY: self.key }
            )
        
        msg_type = CMsgTypes.CREATE_RACE\
                     if self.create_race or self.car_count == 1\
                     else CMsgTypes.JOIN_RACE
        return self.msg(msg_type, { 
            CreateRace.BOT_ID: {
                    CCarIdSpec.NAME: self.bot_name, CCarIdSpec.KEY: self.key
            },
            CreateRace.TRACK_NAME: self.track,
            CreateRace.CAR_COUNT: self.car_count,
            CreateRace.PASSWORD: self.creation_password
        })
    
    def throttle(self, throttle):
        self.msg(CMsgTypes.THROTTLE, throttle)
    
    def ping(self):
        self.msg(CMsgTypes.PING, {})
    
