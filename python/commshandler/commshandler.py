import json
import os

from collections import defaultdict
from msgtokens import CMsgTypes, SResponse, SMsgTypes, SCarIdSpec
from abc import ABCMeta, abstractmethod
from race.race import Race

class CommsHandler(object):
    __metaclass__ = ABCMeta
    LOG_DIRECTORY = './log'
    START_MESSAGES = [
            CMsgTypes.JOIN, CMsgTypes.CREATE_RACE, CMsgTypes.JOIN_RACE,
            SMsgTypes.GAME_INIT, SMsgTypes.GAME_START
    ]
    
    def __init__(self, bot_name, driver_factory, log=False):
        self.driver = driver_factory.create_driver_instance(bot_name, self)
        
        self.log = log
        if log:
            self.initialize_logging()
        
        self.race = None
        self.bot_color = None
    
    def initialize_logging(self):
        self.current_game_id = None
        self.messages_by_game = defaultdict(list)
        self.start_messages = []
        self.end_messages = []
    
    def create_msg_handler(self):
        return {
            SMsgTypes.GAME_START: self.on_game_start,
            SMsgTypes.CAR_POSITIONS: self.on_car_positions,
            SMsgTypes.CRASH: self.on_crash,
            SMsgTypes.GAME_END: self.on_game_end,
            SMsgTypes.ERROR: self.on_error,
            SMsgTypes.GAME_INIT: self.on_game_init,
        }
    
    def log_message(self, msg):
        msg_type = msg[SResponse.MSG_TYPE]
            
        msg_string = json.dumps(msg) + "\n"
        if msg_type in self.START_MESSAGES:
            self.start_messages.append(msg_string)
            return
        if msg_type in SMsgTypes.YOUR_CAR:
            self.start_messages.append(msg_string)
            self.bot_color = msg[SResponse.DATA][SCarIdSpec.COLOR]
            return
        if msg_type == SMsgTypes.CAR_POSITIONS:
            self.current_game_id = msg[SResponse.GAME_ID]
            self.messages_by_game[self.current_game_id].append(msg_string)
            return
        if msg_type == SMsgTypes.GAME_END:
            self.messages_by_game[self.current_game_id].append(msg_string)
            self.current_game_id = None
            return
        if msg_type == SMsgTypes.TOURNAMENT_END:
            self.end_messages.append(msg_string)
            return
        if msg_type == CMsgTypes.PING and not self.current_game_id:
            self.start_messages.append(msg_string)
            return
        
        assert self.current_game_id
        self.messages_by_game[self.current_game_id].append(msg_string)
    
    def write_logged_messages(self):
        if not self.log:
            return
        
        if not os.path.exists(self.LOG_DIRECTORY):
            os.makedirs(self.LOG_DIRECTORY)
        
        for game_id, messages in self.messages_by_game.iteritems():
            file_name = os.path.join(
                    './log', '{0}_{1}'.format(
                            game_id,
                            self.bot_color if self.bot_color else self.bot_name
                    )
            )
            
            with open(file_name, 'w+') as f:
                f.writelines(self.start_messages)
                f.writelines(messages)
                f.writelines(self.end_messages)
        
        self.initialize_logging()
    
    @abstractmethod
    def create_input_source(self):
        pass
    
    def msg_loop(self):
        msg_map = self.create_msg_handler()
        input_file = self.create_input_source()
        line = input_file.readline()
        
        while line:
            msg = json.loads(line)
            msg_type, data = msg[SResponse.MSG_TYPE], msg[SResponse.DATA]
            self.log_message(msg)
            
            if SResponse.GAME_TICK in msg:
                self.game_tick = msg[SResponse.GAME_TICK]
            elif msg_type == SMsgTypes.CAR_POSITIONS:
                self.game_tick = 0
            
            if msg_type in msg_map:
                msg_map[msg_type](data)
            elif msg_type not in CMsgTypes.values:
                print("Got {0}".format(msg_type))
                
                self.ping()
            line = input_file.readline()
        
        self.write_logged_messages()
        input_file.close()
    
    @abstractmethod
    def msg(self, msg_type, data):
        pass
    
    @abstractmethod
    def send(self, msg):
        pass
    
    @abstractmethod
    def join(self):
        pass
    
    @abstractmethod
    def throttle(self, throttle):
        pass
    
    @abstractmethod
    def ping(self):
        pass
    
    def on_game_start(self, data):
        self.driver.on_game_start(data).execute(self)
    
    def on_game_init(self, data):
        self.race = Race(data)
        self.driver.on_game_init(data, self.race)
    
    def on_game_end(self, data):
        print("Race ended")
        self.race.on_game_end(data)
        self.ping()
    
    def on_car_positions(self, data):
        self.driver.on_car_positions(data).execute(self)
        self.race.on_car_positions(data, self.game_tick)
    
    def on_crash(self, data):
        self.driver.on_crash(data).execute(self)
        self.race.on_crash(data, self.game_tick)
    
    def on_error(self, data):
        self.driver.on_error(data).execute(self)
    
    def run(self):
        self.join()
        self.msg_loop()
