import sys
from race.raceoptions import RaceOptions
from commshandler.networkcommshandler import NetworkCommsHandler
from driver.constantthrottledriver import Driver
from driver.driverfactory import DriverFactory
from driver.strategy import ConstantThrottleStrategy
import numpy as np

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        print np.array([1,2,3])
        host, port, name, key = sys.argv[1:5]
        race_options = RaceOptions(name, key)
        strategy = ConstantThrottleStrategy(0.655)
        driver_factory = DriverFactory(Driver, strategy, strategy)
        comms_handler = NetworkCommsHandler(
                host, port, race_options, driver_factory, True
        )
        comms_handler.run()
