from car import Car
from commshandler.msgtokens import RaceSpec, CarSpec, SCarIdSpec, GameInit,\
        RaceSession
from track import Track
from copy import copy

class Race(object):
    def __init__(self, data):
        race_data = data[GameInit.RACE]
        self.laps = race_data[RaceSpec.RACE_SESSION][RaceSession.LAPS]
        self.track = Track(race_data[RaceSpec.TRACK])
        
        self.cars = {}
        for car in race_data[RaceSpec.CARS]:
            car_color = car[CarSpec.ID][SCarIdSpec.COLOR]
            self.cars[car_color] = Car(car, self.track)
        
        self.tick = 0
    
    def on_car_positions(self, positions_data, tick):
        for car in positions_data:
            car_color = car[CarSpec.ID][SCarIdSpec.COLOR]
            self.cars[car_color].on_car_positions(car, tick)
        
        self.tick = tick
    
    def on_crash(self, data, tick):
        crashed_color = data[SCarIdSpec.COLOR]
        self.cars[crashed_color].on_crash(data, tick)
    
    def on_game_end(self, data):
        pass
    
    def copy(self):
        new_copy = copy(self)
        new_copy.cars = {}
        for color, car in self.cars.iteritems():
            new_copy.cars[color] = copy(car)
        
        return new_copy
