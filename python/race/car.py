from commshandler.msgtokens import CarSpec, CarDimensions, SCarIdSpec,\
        PositionSpec, PiecePosition, LanePosition
from collections import namedtuple

CarPosition = namedtuple('CarPosition', ['piece', 'in_piece_distance'])

class CarPosition(object):
    def __init__(self, track, piece_index, in_piece_distance, start_lane_index,
                 end_lane_index, lap, angle):
        self.track = track
        
        self.piece_index = piece_index
        self.in_piece_distance = in_piece_distance
        self.start_lane_index = start_lane_index
        self.end_lane_index = end_lane_index
        self.lap = lap
        self.angle = angle
    
    def distance_to(self, car_position):
        assert self.start_lane_index == car_position.start_lane_index
        assert self.start_lane_index == self.end_lane_index
        
        if self.piece_index == car_position.piece_index:
            distance = car_position.in_piece_distance - self.in_piece_distance
            assert distance >= 0.0
            return distance
        
        start_piece = self.track[self.piece_index][self.start_lane_index]
        start_piece_dist_remaining = start_piece.length - self.in_piece_distance
        end_piece_dist_travelled = car_position.in_piece_distance
        total_distance = start_piece_dist_remaining + end_piece_dist_travelled
        for i in range(self.piece_index + 1, car_position.piece_index):
            total_distance += self.track[i][self.start_lane_index].length
        
        return total_distance

class Car(object):
    def __init__(self, car_spec, track):
        self.track = track
        
        self.name = car_spec[CarSpec.ID][SCarIdSpec.NAME]
        self.color = car_spec[CarSpec.ID][SCarIdSpec.COLOR]
        
        self.guide_flag_pos\
            = car_spec[CarSpec.DIMENSIONS][CarDimensions.GUIDE_FLAG_POSITION]
        self.length = car_spec[CarSpec.DIMENSIONS][CarDimensions.LENGTH]
        self.width = car_spec[CarSpec.DIMENSIONS][CarDimensions.WIDTH]
        
        self.car_position = None
        self.prev_car_position = None
        self.speed = 0.0
        self.prev_speed = 0.0
        self.acceleration = 0.0
        self.last_update_tick = 0

    def on_car_positions(self, position_data, tick):
        assert self.name == position_data[PositionSpec.ID][SCarIdSpec.NAME]
        assert self.color == position_data[PositionSpec.ID][SCarIdSpec.COLOR]
        
        self.update_car_position(position_data, tick)
    
    def on_crash(self, data, tick):
        self.prev_car_position = self.car_position
        self.speed = 0.0
        self.prev_speed = 0.0
        self.acceleration = 0.0
    
    def update_car_position(self, position_data, tick):
        angle = position_data[PositionSpec.ANGLE]
        
        piece_position = position_data[PositionSpec.PIECE_POS]
        in_piece_distance = piece_position[PiecePosition.IN_PIECE_DISTANCE]
        piece_index = piece_position[PiecePosition.PIECE_INDEX]
        lap = piece_position[PiecePosition.LAP]
        
        lane_data = piece_position[PiecePosition.LANE]
        start_lane = lane_data[LanePosition.START]
        end_lane = lane_data[LanePosition.END]
        
        self.prev_car_position = self.car_position
        self.car_position = CarPosition(
                self.track, piece_index, in_piece_distance, start_lane,
                end_lane, lap, angle
        )
        
        dt = float(tick - self.last_update_tick)\
                if tick and self.last_update_tick else 1.0
        self.last_update_tick = tick
        self.update_speed(dt)
        self.update_acceleration(dt)
    
    def update_speed(self, dt):
        if self.prev_car_position and self.car_position:
            self.prev_speed = self.speed
            self.speed\
                = self.prev_car_position.distance_to(self.car_position) / dt
        else:
            self.speed = 0.0
    
    def update_acceleration(self, dt):
        self.acceleration = (self.speed - self.prev_speed) / dt
