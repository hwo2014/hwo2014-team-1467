import math
from commshandler.msgtokens import PieceSpec, TrackSpec, LaneSpec

class Track(object):
    def __init__(self, track_data):
        self._pieces = []
        self.name = track_data[TrackSpec.NAME]
        
        self.create_track_pieces(
                track_data[TrackSpec.PIECES], track_data[TrackSpec.LANES]
        )
    
    def create_track_pieces(self, pieces, lane_specs):
        for track_piece in pieces:
            switch = PieceSpec.SWITCH in track_piece
            
            if PieceSpec.RADIUS in track_piece:
                angle  = track_piece[PieceSpec.ANGLE]
                radius = track_piece[PieceSpec.RADIUS]
                self._pieces.append(
                        CurvedTrackPiece(angle, radius, switch, lane_specs)
                )
            if PieceSpec.LENGTH in track_piece:
                length = track_piece[PieceSpec.LENGTH]
                self._pieces.append(
                        StraightTrackPiece(length, switch, lane_specs)
                )
    
    def __getitem__(self, key):
        return self._pieces[key]

class TrackPiece(object):
    def __init__(self, switch, lanes_list):
        self.switch = switch
        self._lanes = dict(map(lambda lane: (lane.index, lane), lanes_list))
    
    def __getitem__(self, key):
        return self._lanes[key]

class CurvedTrackPiece(TrackPiece):
    def __init__(self, angle, radius, switch, lane_specs):
        self.angle = angle
        self.radius = radius
        
        lanes = map(
                lambda ls: CurvedLane(self.angle, self.radius, ls),
                lane_specs
        )
        super(CurvedTrackPiece, self).__init__(switch, lanes)

class StraightTrackPiece(TrackPiece):
    def __init__(self, length, switch, lane_specs):
        self.length = length
        
        lanes = map(lambda ls: StraightLane(self.length, ls), lane_specs)
        super(StraightTrackPiece, self).__init__(switch, lanes)

class Lane(object):
    def __init__(self, lane_spec):
        self.distance_from_centre = lane_spec[LaneSpec.DISTANCE_FROM_CENTER]
        self.index = lane_spec[LaneSpec.INDEX]
    
class StraightLane(Lane):
    def __init__(self, length, lane_spec):
        super(StraightLane, self).__init__(lane_spec)
        self.length = length

class CurvedLane(Lane):
    def __init__(self, angle, radius, lane_spec):
        super(CurvedLane, self).__init__(lane_spec)
        self.angle = angle
        
        self.is_right_curve = (angle >= 0)
        if self.is_right_curve:
            self.radius = radius - self.distance_from_centre
        else:
            self.radius = radius + self.distance_from_centre
        
        diameter = self.radius * 2
        self.length = abs((math.pi * diameter) * (self.angle / 360.0))
