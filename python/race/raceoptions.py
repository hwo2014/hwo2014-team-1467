class RaceOptions(object):
    def __init__(self, name, key, track=None, car_count=1, index=0,
                 create_race=False, password="bsatest"):
        self.name = name
        self.key = key
        self.car_count = car_count
        self.index = index
        self.create_race = create_race
        self.track = track
        self.password = password
