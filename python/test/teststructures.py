test_track_spec = {
    "pieces": [
      {
        "length": 100.0
      },
      
      {
        "length": 100.0,
        "switch": True
      },
      {
        "angle": 22.5,
        "switch": True,
        "radius": 200
      },
      {
        "angle": -22.5,
        "radius": 200
      },
    ],
    "lanes": [
      {
        "index": 0,
        "distanceFromCenter": -10
      },
      {
        "index": 1,
        "distanceFromCenter": 10
      }
    ],
    "id": "keimola",
    "startingPoint": {
      "position": {
        "y": -44.0,
        "x": -300.0
      },
      "angle": 90.0
    },
    "name": "Keimola"
}
