import unittest

from race.track import Track
from teststructures import test_track_spec

class TestTrackRepr(unittest.TestCase):
    def test_curved_section(self):
        track = Track(test_track_spec)
        
        assert track[0][1].length == 100
        
        assert not track[0].switch
        assert track[1].switch
        assert track[2].switch
        assert not track[0].switch
        
        self.assertAlmostEqual(82.467, track[2][0].length, 3)
        assert 210 == track[2][0].radius
        self.assertAlmostEqual(74.613, track[2][1].length, 3)
        assert 190 == track[2][1].radius
        
        self.assertAlmostEqual(74.613, track[3][0].length, 3)
        assert 190 == track[3][0].radius
        self.assertAlmostEqual(82.467, track[3][1].length, 3)
        assert 210 == track[3][1].radius
