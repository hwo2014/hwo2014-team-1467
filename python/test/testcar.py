import unittest

from teststructures import test_track_spec
from race.track import Track
from race.car import CarPosition

class TestCarPositions(unittest.TestCase):
    def test_distance_to(self):
        track = Track(test_track_spec)
        cp1 = CarPosition(track, 2, 16.7, 0, 0, 1, 22)
        cp2 = CarPosition(track, 3, 111.4, 0, 0, 1, 22)
        
        self.assertAlmostEqual(177.167, cp1.distance_to(cp2), 3)
